package Ro.Orange;

public class Employee {
    private String name = null;
    private int age = 0;

    public Employee(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "EmployeeName:" + getName() + "; EmployeeAge:" + getAge();
    }
}
