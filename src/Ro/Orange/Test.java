package Ro.Orange;

import java.util.Arrays;
import java.util.List;

public class Test {

    public static void main(String[] args) {

         List<Employee> employees = Arrays.asList(
                 new Employee("Alex",32),
                 new Employee("Ioana",30),
                 new Employee("Serban",34),
                 new Employee("Dana",36),
                 new Employee("Catalin",29));

         Employee employee = employees.stream().filter(e ->e.getAge()<30).findAny()
                            .orElse(null);

        System.out.println(employee.toString());

    }
}
